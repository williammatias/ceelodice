package com.qualixium.ceelodice.android;

import android.content.Context;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import java.util.HashMap;

/**
 *
 * @author William Matias
 */


public class Global {

    public static String ANALYTICS_PROPERTY_ID = "UA-37659676-13";

    private Global() {
    }

    public static Global getInstance() {
        return GlobalHolder.INSTANCE;
    }

    private static class GlobalHolder {

        private static final Global INSTANCE = new Global();
    }

    public enum TrackerName {

        APP_TRACKER
    }

    public static HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public static synchronized Tracker getTracker(TrackerName trackerId, Context context) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
            if (trackerId == TrackerName.APP_TRACKER) {
                mTrackers.put(trackerId, analytics.newTracker(ANALYTICS_PROPERTY_ID));
            }

        }
        return mTrackers.get(trackerId);
    }
}